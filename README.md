# Install Docker CE on RHEL 7.6 

### The script does the following: 

Update system:
```
sudo yum update -y
```

Install required packages:
```
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```

Install container-selinux. Check for latest version: http://mirror.centos.org/centos/7/extras/x86_64/Packages/
```
sudo yum install -y  http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```

Set up Docker repository:
```
sudo yum-config-manager --add-repo \ https://download.docker.com/linux/centos/docker-ce.repo
```

Fix path issue with repository:
```
sudo sed -i 's/$releasever/7/g' /etc/yum.repos.d/docker-ce.repo 
```

Install docker:
```
sudo yum install -y docker-ce docker-ce-cli containerd.io
```

Add current user to docker group:
```
sudo usermod -a -G docker $USER
```

Start and persist through system boot:
```
sudo systemctl start docker
sudo systemctl enable docker
```
